from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

from .models import Attendee
from events.models import Conference

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        # Get all of the attendees
        attendees = Attendee.objects.filter(conference=conference_id)

        #Turn the attendees into names and urls
        response = []
        for attendee in attendees:
            response.append({
                "name": attendee.title,
                "href": attendee.get_api_url()
            })

        #Return the attendees in a JsonResponse
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            )
    else:
        #Parse that data in the body of the request
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
                )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


def api_show_attendee(request, id):
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": attendee.conference.name,
                "href": attendee.conference.get_api_url(),
            },
        }
    )
