from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import State

from .models import Conference, Location

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        #Get all of the conferences
        conferences = Conference.objects.all()

        #Turn the conferences into names and urls
        response = []
        for conference in conferences:
            response.append({
                "name": conference.name,
                "href": conference.get_api_url(),
            })
        #Return the conferences in a JsonResponse
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        #Parse that data in the body of the request
        content = json.loads(request.body)

        #Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        # Use that Json data to create a location
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
    )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        # Get all of the conferences
        conference = Conference.objects.get(id=id)
        # Return all the confereneces in JsonResponse
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        # Get the specific conference I am interested in
        # delete it
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        # Get all of the locations
        locations = Location.objects.all()

        # Turn the locations into names, city, room_number, etc
        response = []
        for location in locations:
            response.append({
                "name": location.name,
                "city": location.city,
                "room_count": location.room_count,
                "state": location.state,
            })
        # Return all the locations in JsonResponse
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        #Parse that data in the body of the request
        content = json.loads(request.body)

        #(ExampleSpecific)
        # Get the state object and put it in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # Use that Json data to create a location
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        #Get all of the locations
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        #Parse that data in the body of the request
        content = json.loads(request.body)

        # Get the state object and put it in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        # Use that Json data to create a location
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        # Return the JsonResponse
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
